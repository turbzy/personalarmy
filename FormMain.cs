﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Awesomium.Core;
using Awesomium.Windows.Forms;

namespace PersonalArmy
{
    public partial class FormMain : Form
    {
        private string _rootDataPath;

        private List<Category> _categories;

        private Category _selectedCategory;
        private Account _selectedAccount;

        public FormMain()
        {
            _rootDataPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + Path.DirectorySeparatorChar + "PersonalArmy";

            _categories = new List<Category>();

            this.FormClosing += FormMain_FormClosing;

            InitializeComponent();

            LoadConfiguration();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (Category category in _categories)
            {
                foreach (Account account in category.Accounts)
                {
                    if (account.Browser != null)
                    {
                        account.Browser.Dispose();
                    }
                    if (account.Session != null)
                    {
                        account.Session.Dispose();
                    }
                }
            }

            SaveConfiguration();
        }

        void SaveConfiguration()
        {
            StreamWriter sw = new StreamWriter(File.OpenWrite(_rootDataPath + Path.DirectorySeparatorChar + "PersonalArmy.db"));

            sw.BaseStream.SetLength(0);

            sw.WriteLine("BeginConfiguration:");   


            foreach (Category category in _categories)
            {
                sw.WriteLine("\tBeginCategory:");
                sw.WriteLine("\t\tName:" + category.Name);
                sw.WriteLine("\t\tDataPath:" + category.DataPath);
                
                foreach (Account account in category.Accounts)
                {
                    sw.WriteLine("\t\tBeginAccount:");
                    sw.WriteLine("\t\t\tName:" + account.Name);
                    sw.WriteLine("\t\t\tDataPath:" + account.DataPath);
                    sw.WriteLine("\t\t\tURL:" + account.URL);
                    sw.WriteLine("\t\tEndAccount:");
                }

                sw.WriteLine("\tEndCategory:");
            }

            sw.WriteLine("EndConfiguration:");

            sw.Flush();
            sw.Close();
        }

        string[] GetConfigurationLine(StreamReader sr)
        {
            return sr.ReadLine().Trim().Split(new char[] {':'}, 2);
        }

        void LoadConfiguration()
        {
            try
            {
                StreamReader sr = new StreamReader(File.OpenRead(_rootDataPath + Path.DirectorySeparatorChar + "PersonalArmy.db"));

                string[] line;
                while (true)
                {
                    line = GetConfigurationLine(sr);

                    if (line[0] == "BeginCategory")
                    {
                        Category category = null;
                        string categoryName = "";
                        string categoryDataPath = "";

                        while (true)
                        {
                            line = GetConfigurationLine(sr);

                            if (line[0] == "Name")
                            {
                                categoryName = line[1];
                            }
                            else if (line[0] == "DataPath")
                            {
                                categoryDataPath = line[1];

                                category = AddCategory(categoryName, categoryDataPath);
                            }
                            else if (line[0] == "BeginAccount")
                            {
                                string accountName = "";
                                string accountDataPath = "";
                                string accountURL = "";

                                while (true)
                                {
                                    line = GetConfigurationLine(sr);

                                    if (line[0] == "Name")
                                    {
                                        accountName = line[1];
                                    }
                                    else if (line[0] == "DataPath")
                                    {
                                        accountDataPath = line[1];
                                    }
                                    else if (line[0] == "URL")
                                    {
                                        accountURL = line[1];
                                    }
                                    else if (line[0] == "EndAccount")
                                    {
                                        AddAccount(accountName, accountDataPath, accountURL, category);
                                        break;
                                    }
                                }
                            }
                            else if (line[0] == "EndCategory")
                            {
                                break;
                            }
                        }
                    }
                    else if (line[0] == "EndConfiguration")
                    {
                        break;
                    }
                }

                sr.Close();
            } catch (FileNotFoundException)
            {

            }
            
        }

        public void InitializeBrowser(Account account)
        {
            account.Session = WebCore.CreateWebSession(_rootDataPath + Path.DirectorySeparatorChar + account.Category.DataPath + Path.DirectorySeparatorChar + account.DataPath, WebPreferences.Default);
            account.Browser = new WebControl();
            account.Browser.Dock = DockStyle.Fill;
            account.Browser.WebSession = account.Session;
            account.Browser.Source = new Uri(account.URL);
            account.Browser.AddressChanged += Browser_AddressChanged;
            
        }

        private void Browser_AddressChanged(object sender, UrlEventArgs e)
        {
            textBox1.Text = ((WebControl)sender).Source.ToString();
        }

        private void newCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string categoryName = Prompt.ShowNewCategoryDialog();

            AddCategory(categoryName, categoryName.Replace(" ", ""));
        }

        private void newAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_selectedCategory != null)
            {
                string[] accountData = Prompt.ShowNewAccountDialog();
                string accountName = accountData[0];
                string accountURL = accountData[1];

                AddAccount(accountName, accountName.Replace(" ", ""), accountURL, _selectedCategory);
            }
        }

        void OrganizeCategories()
        {
            int leftCounter = 0;
            foreach (Control c in this.panelCategories.Controls)
            {
                c.Left = leftCounter;
                leftCounter += c.Width + 4;
            }
        }

        Category AddCategory(string name, string datapath)
        {
            Category category = new Category();
            category.Name = name;
            category.DataPath = datapath;
   
            category.Button = new Button();
            category.Button.ContextMenuStrip = this.contextMenuStrip1;
            category.Button.Text = name;
            category.Button.Height = 33;
            category.Button.Width = 168;
            category.Button.Tag = category;
            category.Button.Click += categoryButton_Click;

            _categories.Add(category);
            this.panelCategories.Controls.Add(category.Button);

            if (this.panelCategories.Controls.Contains(label2))
            {
                this.panelCategories.Controls.Remove(label2);
            }

            OrganizeCategories();

            return category;
        }

        Account AddAccount(string name, string datapath, string url, Category category)
        {
            Account account = new Account();
            account.Name = name;
            account.DataPath = datapath;
            account.URL = url;
            account.Category = category;

            account.Button = new Button();
            account.Button.ContextMenuStrip = this.contextMenuStrip1;
            account.Button.Text = name;
            account.Button.Height = 33;
            account.Button.Width = 168;
            account.Button.Tag = account;
            account.Button.Click += accountButton_Click;

            category.Accounts.Add(account);
            category.Button.PerformClick();

            return account;
        }


        private void categoryButton_Click(object sender, EventArgs e)
        {
            _selectedCategory = (Category)((Button)sender).Tag;

            int topCounter = 0;

            panelAccounts.Controls.Clear();
            foreach (Account account in _selectedCategory.Accounts)
            {
                account.Button.Top = topCounter;
                panelAccounts.Controls.Add(account.Button);
                topCounter += account.Button.Height + 10;
            }

            if (topCounter == 0)
            {
                Label label = new Label();
                label.AutoSize = true;
                label.Text = "(you have no accounts\n in this category. \nchoose File->New Account)";
                panelAccounts.Controls.Add(label);
            }
        }

        private void accountButton_Click(object sender, EventArgs e)
        {
            _selectedAccount = (Account)((Button)sender).Tag;

            if (_selectedAccount.Browser == null)
            {
                InitializeBrowser(_selectedAccount);
            }

            this.panelBrowser.Controls.Clear();
            this.panelBrowser.Controls.Add(_selectedAccount.Browser);
            _selectedAccount.Browser.Focus();
        }

        private void configurationAutoSaveTimer_Tick(object sender, EventArgs e)
        {
            SaveConfiguration();
        }

        private void buttonHome_Click(object sender, EventArgs e)
        {
            _selectedAccount.Browser.Source = new Uri(_selectedAccount.URL);
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            _selectedAccount.Browser.Reload(false);
        }

        void CleanupBrowser(Account account)
        {
            if (account.Browser != null)
            {
                if (account.Browser.Parent != null)
                {
                    account.Browser.Parent.Controls.Remove(account.Browser);
                }
                account.Browser.Dispose();
                account.Session.Dispose();
                account.Browser = null;
                account.Session = null;
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Try to cast the sender to a ToolStripItem
            ToolStripItem menuItem = sender as ToolStripItem;
            if (menuItem != null)
            {
                // Retrieve the ContextMenuStrip that owns this ToolStripItem
                ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
                if (owner != null)
                {
                    // Get the control that is displaying this context menu
                    Control sourceControl = owner.SourceControl;


                    if (sourceControl.Tag.GetType() == typeof(Category)) {
                        if (DialogResult.Yes == MessageBox.Show("Are you sure you want to delete this category and all associated accounts?\r\nThis cannot be undone.", "Delete Category", MessageBoxButtons.YesNo))
                        {
                            Category category = (Category)sourceControl.Tag;

                            foreach (Account account in category.Accounts)
                            {
                                CleanupBrowser(account);
                            }

                            if (_selectedCategory == category)
                            {
                                panelAccounts.Controls.Clear();
                                panelBrowser.Controls.Clear();

                                _selectedAccount = null;
                                _selectedCategory = null;
                            }

                            _categories.Remove(category);
                            category.Button.Parent.Controls.Remove(category.Button);
                        }
                    } else if (sourceControl.Tag.GetType() == typeof(Account)) {
                        if (DialogResult.Yes == MessageBox.Show("Are you sure you want to delete this account?\r\nThis cannot be undone.", "Delete Account", MessageBoxButtons.YesNo))
                        {
                            Account account = (Account)sourceControl.Tag;

                            CleanupBrowser(account);

                            if (_selectedAccount == account)
                            {
                                panelBrowser.Controls.Clear();
                                _selectedAccount = null;
                            }

                            account.Category.Accounts.Remove(account);
                            account.Button.Parent.Controls.Remove(account.Button);
                        }
                    }
                }
            }
        }
    }

    public class Category
    {
        public string Name;
        public string DataPath;
        public List<Account> Accounts;
        public Button Button;

        public Category()
        {
            this.Accounts = new List<Account>();
        }
    }
    public class Account
    {
        public Button Button;
        public Category Category;
        public string Name;
        public string URL;
        public string DataPath;
        public WebSession Session;
        public WebControl Browser;
    }
}
