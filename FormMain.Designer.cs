﻿namespace PersonalArmy
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelCategories = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelSelectedCategory = new System.Windows.Forms.Panel();
            this.panelSelectedAccount = new System.Windows.Forms.Panel();
            this.panelBrowser = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonPassword = new System.Windows.Forms.Button();
            this.buttonUsername = new System.Windows.Forms.Button();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonHome = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panelAccounts = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.webSessionProvider1 = new Awesomium.Windows.Forms.WebSessionProvider(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationAutoSaveTimer = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelCategories.SuspendLayout();
            this.panelSelectedCategory.SuspendLayout();
            this.panelSelectedAccount.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelAccounts.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCategories
            // 
            this.panelCategories.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelCategories.Controls.Add(this.label2);
            this.panelCategories.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCategories.Location = new System.Drawing.Point(0, 24);
            this.panelCategories.Name = "panelCategories";
            this.panelCategories.Size = new System.Drawing.Size(1076, 40);
            this.panelCategories.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(260, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "(You have no categories. Choose File->Add Category)";
            // 
            // panelSelectedCategory
            // 
            this.panelSelectedCategory.Controls.Add(this.panelSelectedAccount);
            this.panelSelectedCategory.Controls.Add(this.panelAccounts);
            this.panelSelectedCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSelectedCategory.Location = new System.Drawing.Point(0, 64);
            this.panelSelectedCategory.Name = "panelSelectedCategory";
            this.panelSelectedCategory.Size = new System.Drawing.Size(1076, 618);
            this.panelSelectedCategory.TabIndex = 1;
            // 
            // panelSelectedAccount
            // 
            this.panelSelectedAccount.AutoScroll = true;
            this.panelSelectedAccount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSelectedAccount.Controls.Add(this.panelBrowser);
            this.panelSelectedAccount.Controls.Add(this.panel1);
            this.panelSelectedAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSelectedAccount.Location = new System.Drawing.Point(175, 0);
            this.panelSelectedAccount.Name = "panelSelectedAccount";
            this.panelSelectedAccount.Size = new System.Drawing.Size(901, 618);
            this.panelSelectedAccount.TabIndex = 1;
            // 
            // panelBrowser
            // 
            this.panelBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBrowser.Location = new System.Drawing.Point(0, 31);
            this.panelBrowser.Name = "panelBrowser";
            this.panelBrowser.Size = new System.Drawing.Size(899, 585);
            this.panelBrowser.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonPassword);
            this.panel1.Controls.Add(this.buttonUsername);
            this.panel1.Controls.Add(this.buttonRefresh);
            this.panel1.Controls.Add(this.buttonHome);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(899, 31);
            this.panel1.TabIndex = 3;
            // 
            // buttonPassword
            // 
            this.buttonPassword.Location = new System.Drawing.Point(741, 3);
            this.buttonPassword.Name = "buttonPassword";
            this.buttonPassword.Size = new System.Drawing.Size(96, 25);
            this.buttonPassword.TabIndex = 4;
            this.buttonPassword.Text = "Password";
            this.buttonPassword.UseVisualStyleBackColor = true;
            // 
            // buttonUsername
            // 
            this.buttonUsername.Location = new System.Drawing.Point(639, 3);
            this.buttonUsername.Name = "buttonUsername";
            this.buttonUsername.Size = new System.Drawing.Size(96, 25);
            this.buttonUsername.TabIndex = 3;
            this.buttonUsername.Text = "Username";
            this.buttonUsername.UseVisualStyleBackColor = true;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(558, 3);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(75, 25);
            this.buttonRefresh.TabIndex = 2;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonHome
            // 
            this.buttonHome.Location = new System.Drawing.Point(477, 3);
            this.buttonHome.Name = "buttonHome";
            this.buttonHome.Size = new System.Drawing.Size(75, 25);
            this.buttonHome.TabIndex = 1;
            this.buttonHome.Text = "Home";
            this.buttonHome.UseVisualStyleBackColor = true;
            this.buttonHome.Click += new System.EventHandler(this.buttonHome_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(465, 20);
            this.textBox1.TabIndex = 0;
            // 
            // panelAccounts
            // 
            this.panelAccounts.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAccounts.Controls.Add(this.label1);
            this.panelAccounts.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelAccounts.Location = new System.Drawing.Point(0, 0);
            this.panelAccounts.Name = "panelAccounts";
            this.panelAccounts.Size = new System.Drawing.Size(175, 618);
            this.panelAccounts.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "(choose a category \r\nto view accounts)";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1076, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newCategoryToolStripMenuItem,
            this.newAccountToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newCategoryToolStripMenuItem
            // 
            this.newCategoryToolStripMenuItem.Name = "newCategoryToolStripMenuItem";
            this.newCategoryToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.newCategoryToolStripMenuItem.Text = "New Category";
            this.newCategoryToolStripMenuItem.Click += new System.EventHandler(this.newCategoryToolStripMenuItem_Click);
            // 
            // newAccountToolStripMenuItem
            // 
            this.newAccountToolStripMenuItem.Name = "newAccountToolStripMenuItem";
            this.newAccountToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.newAccountToolStripMenuItem.Text = "New Account";
            this.newAccountToolStripMenuItem.Click += new System.EventHandler(this.newAccountToolStripMenuItem_Click);
            // 
            // configurationAutoSaveTimer
            // 
            this.configurationAutoSaveTimer.Enabled = true;
            this.configurationAutoSaveTimer.Interval = 10000;
            this.configurationAutoSaveTimer.Tick += new System.EventHandler(this.configurationAutoSaveTimer_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(153, 70);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1076, 682);
            this.Controls.Add(this.panelSelectedCategory);
            this.Controls.Add(this.panelCategories);
            this.Controls.Add(this.menuStrip1);
            this.Name = "FormMain";
            this.Text = "PersonalArmy";
            this.panelCategories.ResumeLayout(false);
            this.panelCategories.PerformLayout();
            this.panelSelectedCategory.ResumeLayout(false);
            this.panelSelectedAccount.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelAccounts.ResumeLayout(false);
            this.panelAccounts.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelCategories;
        private System.Windows.Forms.Panel panelSelectedCategory;
        private System.Windows.Forms.Panel panelSelectedAccount;
        private System.Windows.Forms.Panel panelAccounts;
        private Awesomium.Windows.Forms.WebSessionProvider webSessionProvider1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newAccountToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer configurationAutoSaveTimer;
        private System.Windows.Forms.Panel panelBrowser;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonPassword;
        private System.Windows.Forms.Button buttonUsername;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonHome;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    }
}

